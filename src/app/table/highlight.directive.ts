import { Directive, ElementRef, HostListener, Input} from '@angular/core';

@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective {

  constructor(private el: ElementRef) { }

    @HostListener('mouseenter') onMouseEnter(){
      this.highlight('#624F82','white');
    }

    @HostListener('mouseleave') onMouseLeave(){
      this.highlight('','black');
    }
    
    private highlight(color:string,textcolor:string){
      this.el.nativeElement.style.backgroundColor = color;
      this.el.nativeElement.style.color = textcolor;
    }
}
