import { LeadingComment } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  usersArray = [
    {
      "id": 1,
      "name": "Sabari",
      
      "phone": "7592015575",
     
      "editFieldName": ''
    },
    {
      "id": 2,
      "name": "Rithika",
    
      "phone": "9845632600",
 
      "editFieldName": ''
    },
    {
      "id": 3,
      "name": "Mammu",
      
      "phone": "8129600369",
      
      "editFieldName": ''
    },
    {
      "id": 4,
      "name": "Salma",
      
      "phone": "9946920902",
      
      "editFieldName": ''
    },
    
  ]
  
  onEdit(item: any,field: string) {
    
    item.editFieldName = field;
    for(let its of this.usersArray){
      if(its.id!=item.id){
        its.editFieldName=''
      }
    }
  
  }
  Edit(item: any,data:any,colName:any) {
    item.editFieldName = '';
    if(colName=="name"){
      item.name=data;
    
    }
    else if(colName=="phone"){
      item.phone=data
    }
  }

}


